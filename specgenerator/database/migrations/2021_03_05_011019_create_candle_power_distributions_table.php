<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCandlePowerDistributionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candle_power_distributions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('specdata_id');
            $table->string('field1', 30)->nullable();
            $table->timestamps();

            $table->foreign('specdata_id')
                ->references('id')
                ->on('specdata')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('candle_power_distributions');
    }
}
