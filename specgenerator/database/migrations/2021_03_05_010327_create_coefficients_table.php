<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoefficientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coefficients', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('specdata_id');
            $table->string('field1', 30)->nullable();
            $table->string('field2', 30)->nullable();
            $table->string('field3', 30)->nullable();
            $table->string('field4', 30)->nullable();
            $table->string('field5', 30)->nullable();
            $table->string('field6', 30)->nullable();
            $table->string('field7', 30)->nullable();
            $table->string('field8', 30)->nullable();
            $table->string('field9', 30)->nullable();
            $table->string('field10', 30)->nullable();
            $table->string('field11', 30)->nullable();
            $table->string('field12', 30)->nullable();
            $table->string('field13', 30)->nullable();
            $table->string('field14', 30)->nullable();
            $table->string('field15', 30)->nullable();
            $table->string('field16', 30)->nullable();
            $table->string('field17', 30)->nullable();
            $table->string('field18', 30)->nullable();
            $table->timestamps();

            $table->foreign('specdata_id')
                ->references('id')
                ->on('specdata')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coefficients');
    }
}
