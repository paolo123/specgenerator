<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCandelaTabulationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candela_tabulations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('specdata_id');
            $table->string('line_number', 30)->nullable();
            $table->string('field1', 30)->nullable();
            $table->timestamps();

            $table->foreign('specdata_id')
                ->references('id')
                ->on('specdata')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('candela_tabulations');
    }
}
