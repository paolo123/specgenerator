<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLumenZonesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lumen_zones', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('specdata_id');
            $table->string('zone', 30)->nullable();
            $table->string('lumens', 30)->nullable();
            $table->timestamps();

            $table->foreign('specdata_id')
                ->references('id')
                ->on('specdata')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lumen_zones');
    }
}
