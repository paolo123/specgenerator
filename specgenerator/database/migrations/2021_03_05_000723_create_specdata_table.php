<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpecdataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('specdata', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('series_id', 30);
            $table->string('title', 150);
            $table->string('description', 200);
            $table->string('wattage', 30)->nullable();
            $table->string('lumen', 30)->nullable();
            $table->string('cri', 30)->nullable();
            $table->string('efficacy', 30)->nullable();
            $table->string('cct', 30)->nullable();
            $table->string('spacing', 30)->nullable();
            $table->string('test_number', 100)->nullable();
            $table->integer('sequence')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('specdata');
    }
}
