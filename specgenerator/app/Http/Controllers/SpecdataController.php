<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\ZonalLumenSummary;
use App\Models\Luminance;
use App\Models\LumenZone;
use App\Models\CandelaTabulation;
use App\Models\CandlePowerDistribution;
use App\Models\Coefficient;
use App\Models\SpecData;
use Codedge\Fpdf\Facades\Fpdf;
use App\Models\Pdf;

class SpecdataController extends Controller
{
    public function __construct(Pdf $pdf)
    {
        $this->pdf = $pdf;
    }


    public function index(Request $request){
        $specData   = [];
        $seriesId   = $request->id;
        $arrData    = $request->arrData;

        if (isset($seriesId)){
            $specData = SpecData::where('series_id', $seriesId)->get();
        }
        
        return view('specdata.index', ['seriesId' => $seriesId, 'specData' => $specData, 'arrData' => $arrData]);
    }

    public function upload(Request $request){
        global $mSpecData;
        $currentSection = '';
        $seriesId = 'RL531';

        // file is the name of the input box
        if ($request->hasFile('uploadedFile')){
            $request->validate([
                'uploadedFile' => 'required|mimes:txt,pdf,php,rtf|max:2048',
            ]);
            
            $mSpecData      = new SpecData;
            $uploadedFile   = $request->uploadedFile;           // Get uploaded file from request
            $filename       = $uploadedFile->store('images');   // Upload file first to server

            // Get reference to the uploaded file
            $fileContents = Storage::get($filename);    

            // Delete the temporary file
            Storage::delete($filename);     

            // Split the text content
            $arrContents = explode(PHP_EOL, $fileContents);

            // Read each line of the text content
            for($ctr=0;$ctr<count($arrContents);$ctr++){
                $content         = trim($arrContents[$ctr]);
                $currentSection = $this->sectionName($currentSection, $content);
                $currentSection = $this->saveSectionToDB($currentSection, $content, $mSpecData, $seriesId);
            }
        }

        $arrData = array(
            'message' => 'Successfully Saved',
            'filename' => $uploadedFile->getClientOriginalName()
        );

        // Redirect to the index function of this controller
        return redirect()->route('specdata-index', ['id' => $seriesId, 'arrData' => $arrData])->with('arrData', $arrData);
    }

    public function createPdf(Request $request){
        $GLOBALS["headerData"] = array(
            'seriesId' => 'HH6-LED',
            'description' => '6" inch Architectural Lumen LED Downlight',
            'apperture' => '6"',
        );


        $pathFile = storage_path(). '/recipe.pdf';
        $this->pdf->SetMargins(0, 8, 0);
        $this->pdf->AddPage();
        $this->pdf->SetFont('Times','',12);

        for($i=1;$i<=20;$i++)
            $this->pdf->Cell(0,10,'Printing line number '.$i,0,1);

        $this->pdf->Output('F', $pathFile);
        $headers = ['Content-Type' => 'application/pdf'];

        return response()->file($pathFile, $headers);
    }

    // Private Functions

    function deleteSpecData($seriesId, $title){
        $specData = SpecData::where('series_id', $seriesId)
            ->where('title', $title);

        $specData->delete();
    }

    function returnView($arrData){
        return view('specdata.index', ['arrData' => $arrData]);
    }

    function sectionName($section, $content){
        if (strpos(strtoupper($content), 'TITLE -->') !== false){
            $section = 'TITLE';
        }elseif (strpos(strtoupper($content), 'WATTAGE -->') !== false){
            $section = 'WATTAGE';
        }elseif (strpos(strtoupper($content), 'LUMEN -->') !== false){
            $section = 'LUMEN';
        }elseif (strpos(strtoupper($content), 'CRI -->') !== false){
            $section = 'CRI';
        }elseif (strpos(strtoupper($content), 'EFFICACY -->') !== false){
            $section = 'EFFICACY';
        }elseif (strpos(strtoupper($content), 'CCT -->') !== false){
            $section = 'CCT';
        }elseif (strpos(strtoupper($content), 'SPACING -->') !== false){
            $section = 'SPACING';
        }elseif (strpos(strtoupper($content), 'TEST -->') !== false){
            $section = 'TEST';
        }elseif (strpos(strtoupper($content), 'ZONAL LUMENS SUMMARY') !== false){
            $section = 'ZONAL LUMENS SUMMARY';
        }elseif (strpos(strtoupper($content), 'LUMINANCE') !== false){
            $section = 'LUMINANCE';
        }elseif (strpos(strtoupper($content), 'LUMENS PER ZONE') !== false){
            $section = 'LUMENS PER ZONE';
        }elseif (strpos(strtoupper($content), 'CANDELA TABULATION') !== false){
            $section = 'CANDELA TABULATION';
        }elseif (strpos(strtoupper($content), 'CANDLE POWER DISTRIBUTION') !== false){
            $section = 'CANDLE POWER DISTRIBUTION';
        }elseif (strpos(strtoupper($content), 'COEFFICIENTS OF UTILIZATION') !== false){
            $section = 'COEFFICIENTS OF UTILIZATION';
        }elseif($content == ''){
            $section = '';
        }

        return $section;
    }

    function saveSectionToDB($mCurrentSection, $content, $specData, $seriesId){
        if ($mCurrentSection == 'ZONAL LUMENS SUMMARY'){
            $this->saveZonalLumensSummary($content, $specData);
        }elseif($mCurrentSection == 'LUMINANCE'){
            $this->saveLuminance($content, $specData);
        }elseif($mCurrentSection == 'LUMENS PER ZONE'){
            $this->saveLumensPerZone($content, $specData);
        }elseif($mCurrentSection == 'CANDELA TABULATION'){
            $this->saveCandelaTabulation($content, $specData);
        }elseif($mCurrentSection == 'CANDLE POWER DISTRIBUTION'){
            $this->saveCandlePowerDistribution($content, $specData);
        }elseif($mCurrentSection == 'COEFFICIENTS OF UTILIZATION'){
            $this->saveCoefficients($content, $specData);
        }elseif($mCurrentSection == 'TITLE'){
            $this->saveTitle($content, $specData, $seriesId);
            $mCurrentSection = '';
        }elseif($mCurrentSection == 'WATTAGE'){
            $this->saveWattage($content, $specData);
            $mCurrentSection = '';
        }elseif($mCurrentSection == 'LUMEN'){
            $this->saveLumen($content, $specData);
            $mCurrentSection = '';
        }elseif($mCurrentSection == 'CRI'){
            $this->saveCri($content, $specData);
            $mCurrentSection = '';
        }elseif($mCurrentSection == 'EFFICACY'){
            $this->saveEfficacy($content, $specData);
            $mCurrentSection = '';
        }elseif($mCurrentSection == 'CCT'){
            $this->saveCct($content, $specData);
            $mCurrentSection = '';
        }elseif($mCurrentSection == 'SPACING'){
            $this->saveSpacing($content, $specData);
            $mCurrentSection = '';
        }elseif($mCurrentSection == 'TEST'){
            $this->saveTestNumber($content, $specData);
            $mCurrentSection = '';
        }
        return $mCurrentSection;
    }

    function saveCandelaTabulation($data, $specData){
        if (strpos(strtoupper($data), 'CANDELA TABULATION') !== false) return;

        $arrData    = preg_split('/\s+/', $data);
        $zonal      = new CandelaTabulation;

        $zonal->specdata_id = $specData->id;
        $zonal->line_number = '0';
        $zonal->field1 = $arrData[0];

        $zonal->save();
    }

    function saveCandlePowerDistribution($data, $specData){
        if (strpos(strtoupper($data), 'CANDLE POWER DISTRIBUTION') !== false) return;

        $arrData    = preg_split('/\s+/', $data);
        $zonal      = new CandlePowerDistribution;

        $zonal->specdata_id = $specData->id;
        $zonal->field1 = $arrData[0];

        $zonal->save();
    }

    function saveCct($data, $mSpecData){
        $arrData = preg_split('/\s+/', $data);

        $mSpecData->cct = $arrData[2];
        $mSpecData->save();
    }

    function saveCoefficients($data, $specData){
        if (strpos(strtoupper($data), 'COEFFICIENTS OF UTILIZATION') !== false) return;

        $arrData    = preg_split('/\s+/', $data);
        $zonal      = new Coefficient;

        $zonal->specdata_id = $specData->id;
        $zonal->field1 = $arrData[0];
        $zonal->field2 = $arrData[1];
        $zonal->field3 = $arrData[2];
        $zonal->field4 = $arrData[3];
        $zonal->field5 = $arrData[4];
        $zonal->field6 = $arrData[5];
        $zonal->field7 = $arrData[6];
        $zonal->field8 = $arrData[7];
        $zonal->field9 = $arrData[8];
        $zonal->field10 = $arrData[9];
        $zonal->field11 = $arrData[10];
        $zonal->field12 = $arrData[11];
        $zonal->field13 = $arrData[12];
        $zonal->field14 = $arrData[13];
        $zonal->field15 = $arrData[14];
        $zonal->field16 = $arrData[15];
        $zonal->field17 = $arrData[16];
        $zonal->field18 = $arrData[17];

        $zonal->save();
    }

    function saveCri($data, $mSpecData){
        $arrData = preg_split('/\s+/', $data);

        $mSpecData->cri = $arrData[2];
        $mSpecData->save();
    }

    function saveEfficacy($data, $mSpecData){
        $arrData = preg_split('/\s+/', $data);

        $mSpecData->efficacy = $arrData[2];
        $mSpecData->save();
    }

    function saveLuminance($data, $specData){
        if (strpos(strtoupper($data), 'LUMINANCE') !== false) return;

        $arrData    = preg_split('/\s+/', $data);
        $zonal      = new Luminance;

        $zonal->specdata_id = $specData->id;
        $zonal->angle_degrees = '45';
        $zonal->ave1 = $arrData[0];
        $zonal->ave2 = $arrData[1];
        $zonal->ave3 = $arrData[2];

        $zonal->save();
    }

    function saveLumen($data, $mSpecData){
        $arrData = preg_split('/\s+/', $data);

        $mSpecData->lumen = $arrData[2];
        $mSpecData->save();
    }

    function saveLumensPerZone($data, $specData){
        if (strpos(strtoupper($data), 'LUMENS PER ZONE') !== false) return;

        $arrData    = preg_split('/\s+/', $data);
        $zonal      = new LumenZone;

        $zonal->specdata_id = $specData->id;
        $zonal->zone = '0-10';
        $zonal->lumens = $arrData[0];

        $zonal->save();
    }

    function saveSpacing($data, $mSpecData){
        $arrData = preg_split('/\s+/', $data);

        $mSpecData->spacing = $arrData[2];
        $mSpecData->save();
    }

    function saveTestNumber($data, $mSpecData){
        $arrData = preg_split('/\s+/', $data);

        $mSpecData->test_number = $arrData[2];
        $mSpecData->save();
    }

    function saveTitle($data, $mSpecData, $seriesId){
        $arrData    = preg_split('/\s+/', $data);
        $title      = $arrData[2];

        // Delete any existing data if SeriesID and Title exists in DB to prevent duplicate
        $this->deleteSpecData($seriesId, $title);

        $mSpecData->series_id = strtoupper($seriesId);
        $mSpecData->title = strtoupper($title);
        $mSpecData->description = '6" Architectural High Lumen LED Downlight';
        $mSpecData->save();
    }

    function saveWattage($data, $mSpecData){
        $arrData = preg_split('/\s+/', $data);

        $mSpecData->wattage = $arrData[2];
        $mSpecData->save();
    }

    function saveZonalLumensSummary($data, $specData){
        if (strpos(strtoupper($data), 'ZONAL LUMENS SUMMARY') !== false) return;

        $arrData    = preg_split('/\s+/', $data);
        $zonal      = new ZonalLumenSummary;

        $zonal->specdata_id = $specData->id;
        $zonal->zone = '0-20';
        $zonal->lumens = $arrData[0];
        $zonal->lamp_percent = $arrData[1];
        $zonal->fixture_percent = $arrData[2];

        $zonal->save();
    }

    
}
