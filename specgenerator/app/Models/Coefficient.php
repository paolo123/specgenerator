<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Coefficient extends Model
{
    use HasFactory;

    protected $table = 'coefficients';


    // Relationships

    public function specData(){
        return $this->belongsTo(SpecData::class);
    }
}
