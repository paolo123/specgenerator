<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CandelaTabulation extends Model
{
    use HasFactory;

    protected $table = 'candela_tabulations';

    
    
    // Relationships

    public function specData(){
        return $this->belongsTo(SpecData::class);
    }
}
