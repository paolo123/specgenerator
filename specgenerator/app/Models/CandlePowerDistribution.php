<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CandlePowerDistribution extends Model
{
    use HasFactory;

    protected $table = 'candle_power_distributions';


    // Relationships

    public function specData(){
        return $this->belongsTo(SpecData::class);
    }
}
