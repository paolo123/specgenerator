<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LumenZone extends Model
{
    use HasFactory;

    protected $table = 'lumen_zones';

    
    
    // Relationships

    public function specData(){
        return $this->belongsTo(SpecData::class);
    }
}
