<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ZonalLumenSummary extends Model
{
    use HasFactory;

    // Specify a custom table name
    protected $table = 'zonal_lumen_summaries';


    // Relationships

    public function specData(){
        return $this->belongsTo(SpecData::class);
    }
}
