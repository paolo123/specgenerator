<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Luminance extends Model
{
    use HasFactory;

    // Specify a custom table name
    protected $table = 'luminances';


    // Relationships

    public function specData(){
        return $this->belongsTo(SpecData::class);
    }
}
