<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SpecData extends Model
{
    use HasFactory;

    protected $table = 'specdata';

    // Relationships

    public function candelaTabulations(){
        return $this->hasMany(CandelaTabulation::class, 'specdata_id', 'id');
    }

    public function powerDistributions(){
        return $this->hasMany(CandlePowerDistribution::class, 'specdata_id', 'id');
    }

    public function coefficients(){
        return $this->hasMany(Coefficient::class, 'specdata_id', 'id');
    }

    public function lumenZones(){
        return $this->hasMany(LumenZone::class, 'specdata_id', 'id');
    }

    public function luminances(){
        return $this->hasMany(Luminance::class, 'specdata_id', 'id');
    }

    public function zonalLumenSummaries(){
        return $this->hasMany(ZonalLumenSummary::class, 'specdata_id', 'id');
    }
}
