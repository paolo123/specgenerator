<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Codedge\Fpdf\Fpdf\Fpdf;
use App\Models\SpecData;


class Pdf extends Fpdf
{
    public function Header()
    {
        // $this->Image(storage_path() . '/logo.jpg',10,6,30);
        $this->Cell(40,12,'',0,0);
        $this->Image('https://media.iuseelite.com/test/maxilume-logo.jpg',7,7,42,0,'JPG');
        $this->SetFont('Arial','B',19);
        $this->Cell(141,6,$GLOBALS['headerData']['seriesId'],0,2,'R');
        $this->SetFont('Arial','',12);
        $this->Cell(141,6,$GLOBALS['headerData']['description'],0,0,'R');
        $this->setXY(183, 8);
        $this->SetFont('Arial','',30);
        $this->Cell(18,11,$GLOBALS['headerData']['apperture'],1,0,'C');
        $this->SetLineWidth(.1);
        $this->Line(0, 23, 215, 23);
        $this->Line(57, 8, 57, 20);
        $this->Ln(20);
    }

    public function Footer()
    {
        $this->SetY(-15);
        $this->SetFont('Arial','I',8);
        $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
    }
}
