@extends('layouts.app')

@section('content')

    <div class="container">
        <br/><br/>

        <?php  
            if (isset($arrData['message'])){
                echo $arrData['message'] . ' - ' . $arrData['filename'];
            }
        ?>
        <form action="/specdata" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-6">
                    <input type="file" name="uploadedFile" class="form-control">
                </div>
                <div class="col-md-6">
                    <button type="submit" class="btn btn-primary">Upload</button>
                    <a class="btn btn-primary" href="{{ URL::to('/specdata/RL531/pdf') }}">Export to PDF</a>
                </div>
            </div>
        </form>
    </div>
    <br /><br />



    <!-- Photometry Table SectionPage -->        

    @foreach($specData as $spec)

        <div id="photometry_table" class="container" style="margin-top: 10px; margin-bottom: 10px;">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <table>
                    <tr>
                        <td class="cell" id="partnumber"  colspan="5"><b>{{ $spec->title }}</b></td>
                        <td class="cell" id="test">TEST NO:<b>{{ $spec->test_number }}</b></td>
                    </tr>
                    <tr>
                        <td class="cell" id="wattage">INPUT WATTS: <b>{{ $spec->wattage }}</b></td>
                        <td class="cell" id="lumens">LUMENS: <b>{{ $spec->lumen }}</b></td>
                        <td class="cell" id="cri">CRI: <b>{{ $spec->cri }}</b></td>
                        <td class="cell" id="efficacy">EFFICACY: <b>{{ $spec->efficacy }}</b></td>
                        <td class="cell" id="cct">CCT: <b>{{ $spec->cct }}</b></td>
                        <td class="cell" id="spacing">SPACING CRITERIA: <b>{{ $spec->spacing }}</b></td>
                    </tr>  
                </table>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-sm-3 col-md-3 col-lg-3">
                        <p style="line-height: .5rem; margin-bottom: 2px; margin-top: 10px; font-size: .7rem;"><b>Candle Power Distribution(Candelas)</b></p>
                        <div style="float: left; margin-top: 38px;">
                            <table>
                                @foreach($spec->powerDistributions as $powerDistribution)
                                    <tr style="font-size: .7rem;">
                                        <td>{{ $powerDistribution->field1 }}</td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                        <div style="float: left;">
                            <img src="https://media.iuseelite.com/test/curve-test.png" alt="test" width="170" height="auto">
                        </div>
                        
                    </div>
                    <div id="zonal_lumens" class="col-sm-3 col-md-3 col-lg-3">
                        <table style="font-size: .5rem; width: 100%;">
                            <tr><b style="font-size: .8rem;">Zonal Lumens Summary</b></tr>
                            <tr style="background-color: gainsboro;">
                                <td class="cell2"><b>Zone</b></td>
                                <td class="cell2"><b>Lumens</b></td>
                                <td class="cell2"><b>%Lamps</b></td>
                                <td class="cell2"><b>%Fixt</b></td>
                            </tr>
                            @foreach($spec->zonalLumenSummaries as $zonalLumenSummary)
                                <tr>
                                    <td class="cell2">{{ $zonalLumenSummary->zone }}</td>
                                    <td class="cell2">{{ $zonalLumenSummary->lumens }}</td>
                                    <td class="cell2">{{ $zonalLumenSummary->lamp_percent }}</td>
                                    <td class="cell2">{{ $zonalLumenSummary->fixture_percent }}</td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                    <div class="col-sm-2 col-md-2 col-lg-2">
                        
                        <table style="font-size: .5rem;">
                            <tr><p  style="line-height: .5rem; margin-bottom: 2px; margin-top: 5px;"><b style="font-size: .8rem; line-height:">Luminance</b><br><spam style="font-size: .3rem;">(Average candela/M2)</spam></p></tr>
                            <tr style="background-color: gainsboro;">
                                <td class="cell2"><b>Zone</b></td>
                                <td class="cell2"><b>Lumens</b></td>
                                <td class="cell2"><b>%Lamps</b></td>
                                <td class="cell2"><b>%Fixt</b></td>
                            </tr>
                            @foreach($spec->luminances as $luminance)
                                <tr>
                                    <td class="cell2">{{ $luminance->angle_degrees  }}</td>
                                    <td class="cell2">{{ $luminance->ave1  }}</td>
                                    <td class="cell2">{{ $luminance->ave2  }}</td>
                                    <td class="cell2">{{ $luminance->ave3  }}</td>
                                </tr>
                            @endforeach
                            
                        </table>
                    
                    </div>
                    <div class="col-sm-2 col-md-2 col-lg-2">
                        
                        <table style="font-size: .5rem;">
                            <tr><b style="font-size: .8rem;">Lumens Per Zone</b></tr>
                            <tr style="background-color: gainsboro;">
                                <td class="cell2"><b>Zone</b></td>
                                <td class="cell2"><b>Lumens</b></td>
                            </tr>
                            @foreach($spec->lumenZones as $lumenZone)
                                <tr>
                                    <td class="cell2">{{ $lumenZone->zone }}</td>
                                    <td class="cell2">{{ $lumenZone->lumens }}</td>
                                </tr>
                            @endforeach
                            
                        </table>
                        
                    </div>
                    <div class="col-sm-2 col-md-2 col-lg-2">
                        
                        <table style="font-size: .5rem;">
                            <tr><b style="font-size: .8rem;">Candela Tabulation</b></tr>
                            <tr style="background-color: gainsboro;">
                                <td class="cell2"><b>Zone</b></td>
                                <td class="cell2"><b>Lumens</b></td>
                            </tr>
                            @foreach($spec->candelaTabulations as $candelaTabulation)
                                <tr>
                                    <td class="cell2">{{ $candelaTabulation->line_number }}</td>
                                    <td class="cell2">{{ $candelaTabulation->field1 }}</td>
                                </tr>
                            @endforeach
                            
                        </table>
                        
                    </div>
                </div> 
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-sm-3 col-md-3 col-lg-3">
                    </div>
                    <div class="col-ms-9 col-md-9 col-lg-9">
                        <table>
                            <tr><p style="font-size: .8rem; margin-bottom: 2px;"><b>Coefficients of Utilization - Zonal Cavity Method</b>&nbsp;&nbsp;Effective Floor Cavity Reflectance 0.20</p></tr>
                            <tr style="background-color:gainsboro;">
                                <td class="cell3 table_ptop"><b>RC</b></td>
                                <td class="cell3 table_ptop"></td>
                                <td class="cell3 table_ptop"></td>
                                <td class="cell3 table_ptop">80%</td>
                                <td class="cell3 table_ptop"></td>
                                <td class="cell3 table_ptop">70%</td>
                                <td class="cell3 table_ptop"></td>
                                <td class="cell3 table_ptop"></td>
                                <td class="cell3 table_ptop"></td>
                                <td class="cell3 table_ptop">50%</td>
                                <td class="cell3 table_ptop"></td>
                                <td class="cell3 table_ptop"></td>
                                <td class="cell3 table_ptop">30%</td>
                                <td class="cell3 table_ptop"></td>
                                <td class="cell3 table_ptop"></td>
                                <td class="cell3 table_ptop">10%</td>
                                <td class="cell3 table_ptop"></td>
                                <td class="cell3 table_ptop"></td>
                                <td class="cell3 table_ptop">0%</td>
                            </tr>
                            <tr style="background-color:gainsboro;">
                                <td class="cell3 table_pbottom"><b>RW</b></td>
                                <td class="cell3 table_pbottom"><b>70%</b></td>
                                <td class="cell3 table_pbottom"><b>50%</b></td>
                                <td class="cell3 table_pbottom"><b>30%</b></td>
                                <td class="cell3 table_pbottom"><b>10%</b></td>
                                <td class="cell3 table_pbottom"><b>70%</b></td>
                                <td class="cell3 table_pbottom"><b>50%</b></td>
                                <td class="cell3 table_pbottom"><b>30%</b></td>
                                <td class="cell3 table_pbottom"><b>10%</b></td>
                                <td class="cell3 table_pbottom"><b>50%</b></td>
                                <td class="cell3 table_pbottom"><b>30%</b></td>
                                <td class="cell3 table_pbottom"><b>10%</b></td>
                                <td class="cell3 table_pbottom"><b>50%</b></td>
                                <td class="cell3 table_pbottom"><b>30%</b></td>
                                <td class="cell3 table_pbottom"><b>10%</b></td>
                                <td class="cell3 table_pbottom"><b>50%</b></td>
                                <td class="cell3 table_pbottom"><b>30%</b></td>
                                <td class="cell3 table_pbottom"><b>10%</b></td>
                                <td class="cell3 table_pbottom"><b>0%</b></td>
                            </tr>
                            @foreach($spec->coefficients as $coefficient)
                                <tr>
                                    <td class="cell3">{{ $coefficient->field1 }}</td>
                                    <td class="cell3">{{ $coefficient->field2 }}</td>
                                    <td class="cell3">{{ $coefficient->field3 }}</td>
                                    <td class="cell3">{{ $coefficient->field4 }}</td>
                                    <td class="cell3">{{ $coefficient->field5 }}</td>
                                    <td class="cell3">{{ $coefficient->field6 }}</td>
                                    <td class="cell3">{{ $coefficient->field7 }}</td>
                                    <td class="cell3">{{ $coefficient->field8 }}</td>
                                    <td class="cell3">{{ $coefficient->field9 }}</td>
                                    <td class="cell3">{{ $coefficient->field10 }}</td>
                                    <td class="cell3">{{ $coefficient->field11 }}</td>
                                    <td class="cell3">{{ $coefficient->field12 }}</td>
                                    <td class="cell3">{{ $coefficient->field13 }}</td>
                                    <td class="cell3">{{ $coefficient->field14 }}</td>
                                    <td class="cell3">{{ $coefficient->field15 }}</td>
                                    <td class="cell3">{{ $coefficient->field16 }}</td>
                                    <td class="cell3">{{ $coefficient->field17 }}</td>
                                    <td class="cell3">{{ $coefficient->field18 }}</td>
                                    <td class="cell3">{{ $coefficient->field19 }}</td>
                                </tr>
                            @endforeach
                            
                        </table>
                    </div>
                </div>
            </div>
        </div>

    @endforeach

    <!-- End Photometry Table Section Page-->

@endsection