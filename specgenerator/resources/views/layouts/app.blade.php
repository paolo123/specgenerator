<!DOCTYPE html>
<html>
    <head>
        <title>Specification Sheets Data Import</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">

        <style>
            .pagesize{
                
                max-width: 1000px;
                max-height: 1294px;
            }
            
            table {
              font-family: arial, sans-serif;
              border-collapse: collapse;
              width: 100%;
            }
            
            .cell {
              border-top: 1px solid #dddddd;
              border-bottom: 1px solid #dddddd;
              text-align: left;
              padding: 2px 8px 2px 8px;
            }
            
            .cell2 {
              text-align: center;
              padding: 2px;
            }
            .cell3 {
              text-align: center;
              padding: 0px;
              margin: 0px;
              font-size: .7rem;
              line-height: 0.7rem;
            }
            .table_ptop{
                padding-top: 4px;
            }
            .table_pbottom{
                padding-bottom: 4px;
            }
            
            .big_table_padding{
                padding-top: 0px !important;
                padding-bottom: 0px !important;
                padding-left: 10px !important;
            }
          
        </style>  
    </head>
    <body>
        @yield('content')
    </body>
</html>
</html>